﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
//using Microsoft.Office.Interop.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Data.OleDb;


namespace Parsing_File
{    
    public partial class Form1 : Form
    {
        List<string> listFiles = new List<string>();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /*
            byte[] bytes = { 0, 0, 0X12, 0X34};// 00000000 00000000 00000001 00000000 


            // If the system architecture is little-endian (that is, little end first),
            // reverse the byte array.
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);

            int i = BitConverter.ToInt32(bytes, 0);
            Console.WriteLine("int: {0}", i);
            string myHex = i.ToString("X");
            Console.WriteLine("hex: {0:X}", i);
            string binary = Convert.ToString(i, 2);
            Console.WriteLine(binary);

            Int16 shInt = BitConverter.ToInt16(bytes, 1);
            Console.WriteLine("int: {0}", shInt);
            string myHex1 = shInt.ToString("X");
            Console.WriteLine("hex: {0:X}", shInt);
            string binary1 = Convert.ToString(shInt, 2);
            Console.WriteLine(binary1);
            */

        }
        /*
        private void button1_Click(object sender, EventArgs e) 
        {

            /*File Decode */
            /*
            Int32 fileSize = 787456;//Convert.ToInt32(this.FileSize.Text, 16);
            UInt32 startAddress = 0x080C0000;// Convert.ToUInt32(this.StartAddress.Text, 16);
            UInt32 startAddressSec02 = 0x080E0000;
            string strA = "\\";
            string path = String.Concat(strA, listView.FocusedItem.Text);

            new readfile(String.Concat(txtPath.Text, path),
                         fileSize, startAddress);    

        }    */   
        
        /* Save the Excel File in required path */
        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:\";
            saveFileDialog1.Title = "Save Excel Files";

            saveFileDialog1.DefaultExt = "xlsx";
            saveFileDialog1.Filter = "Excel Files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //textBox1.Text = saveFileDialog1.FileName;

            }
            else
            {
                return;
            }
        }    

        /* Browse the S19 file, where S19 file is stored in user system */
        private void btnOpen_Click(object sender, EventArgs e)
        {
            listFiles.Clear();
            listView.Items.Clear();
            using (FolderBrowserDialog fbd = new FolderBrowserDialog() { Description = "Select your path." })
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    txtPath.Text = fbd.SelectedPath;
                    foreach (string item in Directory.GetFiles(fbd.SelectedPath))
                    {
                        // imageList.Images.Add(Icon.ExtractAssociatedIcon(item));
                        imageList.Images.Add(System.Drawing.Icon.ExtractAssociatedIcon(item));
                        FileInfo fi = new FileInfo(item);
                        string searchPattern = "*.S19";

                        if (searchPattern == "*.S19")
                        {
                            listFiles.Add(fi.FullName);
                            listView.Items.Add(fi.Name, imageList.Images.Count - 1);
                        }
                    }
                }
            }

        }

        /* List all the files present in the user selected directory */ 
        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FileInfo fi = new FileInfo();
            var supportedTypes = new[] {"S19"};
            var fileExt = System.IO.Path.GetExtension(listView.FocusedItem.Text).Substring(1);
          //  if (supportedTypes.Contains(fileExt))
            if ((listView.FocusedItem != null) || (supportedTypes.Contains(fileExt)))
            {
               // Process.Start(listFiles[listView.FocusedItem.Index]);
            }
        }
        /*
        private void progressBar1_Click(object sender, EventArgs e)
        {
            ProgressBar pBar = new ProgressBar();
            pBar.Location = new System.Drawing.Point(20, 20);
            pBar.Name = "progressBar1";
            pBar.Width = 200;
            pBar.Height = 30;
            Controls.Add(pBar);
            pBar.Name = "progressBar1";
            pBar.Dock = DockStyle.Bottom;
            pBar.Minimum = 0;
            pBar.Maximum = 100;
            pBar.Value = 70;
            int ProgressToBeupdated = 100;

            BackgroundWorker WorkerThread = new BackgroundWorker();

            WorkerThread.WorkerReportsProgress = true;

            WorkerThread.DoWork += WorkerThread_DoWork;
            WorkerThread.ProgressChanged += WorkerThread_ProgressChanged;

            WorkerThread.RunWorkerAsync(new object());

        }
        void WorkerThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        void WorkerThread_DoWork(object sender, DoWorkEventArgs e)
        {

            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(500);
                (sender as BackgroundWorker).ReportProgress(i);
            }
        } */

        /// Handles the Click event of the btnExport control.    
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void button3_Click(object sender, EventArgs e)
        {
            /*File Decode */
            Int32 fileSize = 787456;//Convert.ToInt32(this.FileSize.Text, 16);
            UInt32 startAddress = 0x080C0000;// Convert.ToUInt32(this.StartAddress.Text, 16);
            UInt32 startAddressSec02 = 0x080E0000;
            string strA = "\\";
            string path = String.Concat(strA, listView.FocusedItem.Text);

            new readfile(String.Concat(txtPath.Text, path),
                         fileSize, startAddress);
            GenerateReport();
        }



        /// Generates the report.
        private static void GenerateReport()
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                //set the workbook properties and add a default sheet in it
                SetWorkbookProperties(p);
                //Create a sheet
                int ix = 0;
                for (int i = 0; i < 52; i++)
                {
                    if (i < 26)
                    {
                        ExcelWorksheet ws = CreateSheet(p, "S19IntensiumFileSec" + (i + 1), (i + 1));
                        DataTable dt = CreateDataTable(i); //My Function which generates DataTable

                        //Merging cells and create a center heading for out table
                        ws.Cells[1, 1].Value = "S19 File";
                        ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        int rowIndex = 2;

                        CreateHeader(ws, ref rowIndex, dt);
                        CreateData(ws, ref rowIndex, dt);
                        //CreateFooter(ws, ref rowIndex, dt);
                    }
                    else
                    {
                        ix = i;
                        int x = i - 26;
                        ExcelWorksheet ws = CreateSheet(p, "S19IntensiumFileSecB" + (x + 1), (ix + 1));
                        DataTable dt = CreateDataTableB(x); //My Function which generates DataTable

                        //Merging cells and create a center heading for out table
                        ws.Cells[1, 1].Value = "S19 File";
                        ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        int rowIndex = 2;

                        CreateHeader(ws, ref rowIndex, dt);
                        CreateData(ws, ref rowIndex, dt);
                        //CreateFooter(ws, ref rowIndex, dt);
                    }
                    

                }

                //MessageBox.Show("Wait for a moment");
                //Generate A File with Random name
                Byte[] bin = p.GetAsByteArray();
                //string file = Guid.NewGuid().ToString() + ".xlsx";
                //string file = "Intensium" + ".xlsx";
                string file = System.IO.Path.GetFileNameWithoutExtension(Program.form1.listView.FocusedItem.Text) + ".xlsx";
                //File.WriteAllBytes(file, bin);

                string strA = "\\";      
                string path = String.Concat(strA, file);//concatenate "\\" with filename

                string pathFile = String.Concat(Program.form1.txtPath.Text, path);// Program.form1.txtPath.Text is directory of excel file
                File.WriteAllBytes(pathFile, bin);  //Copy data to excel file in S19 file directory

                //These lines will open it in Excel
                /* ProcessStartInfo pi = new ProcessStartInfo(pathFile);
                 Process.Start(pi);*/
                // String WOmain = @"C:\Users\tjjtabije\Desktop\TestExcelUpdater\TestUnoREFARM.xlsx";
                /*
                 string filePath = pathFile;
                 string extension = Path.GetExtension(pathFile);
                 string Excel07ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathFile + ";Extended Properties='Excel 12.0 Xml;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text'";

                 string conStr, sheetName;
                 conStr = string.Empty;

                 //Get the name of the First Sheet.
                 using (OleDbConnection con = new OleDbConnection(Excel07ConString))
                 {
                     using (OleDbCommand cmd = new OleDbCommand())
                     {
                         using (OleDbDataAdapter oda = new OleDbDataAdapter())
                         {
                             cmd.Connection = con;
                             con.Open();
                             DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                             //con.Dispose();
                             sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                             DataTable dt1 = new DataTable();
                             //cmd.Connection = con;
                             cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                             //con.Open();
                             oda.SelectCommand = cmd;
                             oda.Fill(dt1);
                             //oda.Dispose();
                             //con.Close();
                             //Populate DataGridView.                            
                             Program.form1.dataGridView1.DataSource = dt1;
                             //Program.form1.label2.Text = dt.Rows.Count.ToString();

                         }
                     }
                 }

                 */
                /*
                string PathConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathFile + ";Extended Properties = \"Excel 12.0 Xml;HDR=YES\"; ";
                OleDbConnection conn = new OleDbConnection(PathConn);
                OleDbDataAdapter myDataAdapter = new OleDbDataAdapter("Select * from[" + file + "$]", conn);
                DataTable dt1 = new DataTable();

                myDataAdapter.Fill(dt1);
                Program.form1.dataGridView1.DataSource = dt1;*/

                //var excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES'", pathFile);
                //Get all worksheet names from the Excel file selected using GetSchema of an OleDbConnection
                string sourceConnectionString = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES'", pathFile);
                OleDbConnection connection = new OleDbConnection(sourceConnectionString);

                connection.Open();
                DataTable tables = connection.GetSchema("Tables", new String[] { null, null, null, "TABLE" });
                connection.Dispose();

                //Add each table name to the combo box
                if (tables != null && tables.Rows.Count > 0)
                {
                    Program.form1.worksheetsComboBox.Items.Clear();
                    foreach (DataRow row in tables.Rows)
                    {
                        Program.form1.worksheetsComboBox.Items.Add(row["TABLE_NAME"].ToString());
                    }
                }
                MessageBox.Show("Select Worksheet from Combobox");
            }
        }   


        private static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName, int ix)
        {
            //ExcelWorksheet ws = p.Workbook.Worksheets.Add
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[ix];

            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            return ws;
        }

        /// <summary>
        /// Sets the workbook properties and adds a default sheet.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        private static void SetWorkbookProperties(ExcelPackage p)
        {
            //Here setting some document properties
            p.Workbook.Properties.Author = "Sharnabasappa";
            p.Workbook.Properties.Title = "EPPlus Sample for Interpreter";


        }

        private static void CreateHeader(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            ws.Cells[2, 1].Value = "Parameters";
            var fill1 = ws.Cells[2, 1].Style.Fill;
            fill1.PatternType = ExcelFillStyle.Solid;
            fill1.BackgroundColor.SetColor(Color.Gray);
            var border1 = ws.Cells[2, 1].Style.Border;
            border1.Bottom.Style = border1.Top.Style = border1.Left.Style = border1.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells[3, 1].Value = "Time Stamp (RTC Value)";
            ws.Cells[4, 1].Value = "BMM Mode";
            ws.Cells[5, 1].Value = "Triggering Event";
            ws.Cells[6, 1].Value = "Event Activation Counter";
            ws.Cells[7, 1].Value = "SOC Min";
            ws.Cells[8, 1].Value = "SOC Max";
            ws.Cells[9, 1].Value = "SOC Avg";
            ws.Cells[10, 1].Value = "SOH";
            ws.Cells[11, 1].Value = "Current Avg High Range";
            ws.Cells[12, 1].Value = "Current Avg Low Range";
            ws.Cells[13, 1].Value = "Amp Throughput High Range";
            ws.Cells[14, 1].Value = "Amp Throughput Low Range";
            ws.Cells[15, 1].Value = "Battery (String) Voltage";
            ws.Cells[16, 1].Value = "Max Cell Voltage";
            ws.Cells[17, 1].Value = "Module Identifier for Vmax";
            ws.Cells[18, 1].Value = "Min Cell Voltage";
            ws.Cells[19, 1].Value = "Module Identifier for Vmin";
            ws.Cells[20, 1].Value = "Max Temp";
            ws.Cells[21, 1].Value = "Module Identifier for Tmax";

            int colIndex = 2;
            foreach (DataColumn dc in dt.Columns) //Creating Headings
            {
                var cell = ws.Cells[rowIndex, colIndex];

                //Setting the background color of header cells to Gray
                var fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.Gray);

                //Setting Top/left,right/bottom borders.
                var border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                //Setting Value in cell                
                int x = Convert.ToInt32(dc.ColumnName);
                cell.Value = "record" + (x + 1);

                colIndex++;
            }
        }

        private static void CreateData(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            int colIndex = 0;
            foreach (DataRow dr in dt.Rows) // Adding Data into rows
            {
                colIndex = 2;
                rowIndex++;

                foreach (DataColumn dc in dt.Columns)
                {
                    var cell = ws.Cells[rowIndex, colIndex];

                    //Setting Value in cell
                    cell.Value = Convert.ToInt64(dr[dc.ColumnName]);

                    //Setting borders of cell
                    var border = cell.Style.Border;
                    border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    colIndex++;
                }
            }
        }




        /// <summary>
        /// Pixel2s the MTU.
        /// </summary>
        /// <param name="pixels">The pixels.</param>
        /// <returns></returns>
        public static int Pixel2MTU(int pixels)
        {
            int mtus = pixels * 9525;
            return mtus;
        }

        /// <summary>
        /// Creates the data table with some dummy data.
        /// </summary>
        /// <returns>DataTable</returns>
        private static DataTable CreateDataTable(int x)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 60; i++)
            {
                dt.Columns.Add(i.ToString());
            }


            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Time_stamp[x, m];
                }
                dt.Rows.Add(dr);
            }


            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Bmm_mode[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Triggering_event[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Event_act_Counter[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Soc_min[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Soc_max[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Soc_avg[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Soh[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Cur_Avg_High_Range[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Cur_Avg_Low_Range[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Amp_throughput_highrange[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Amp_throughput_lowrange[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Battery_voltage[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Max_cell_volt[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Module_Identifier_Vmax[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Min_cell_voltage[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Module_Identifier_Vmin[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Max_temp[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Module_Identifier_Tmax[x, m];
                }
                dt.Rows.Add(dr);
            }


            return dt;
        }


        private static DataTable CreateDataTableB(int x)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 60; i++)
            {
                dt.Columns.Add(i.ToString());
            }


            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Time_stamp[x, m];
                }
                dt.Rows.Add(dr);
            }


            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Bmm_mode[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Triggering_event[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Event_act_Counter[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Soc_min[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Soc_max[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Soc_avg[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Soh[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Cur_Avg_High_Range[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Cur_Avg_Low_Range[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Amp_throughput_highrange[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Amp_throughput_lowrange[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Battery_voltage[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Max_cell_volt[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Module_Identifier_Vmax[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Min_cell_voltage[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Module_Identifier_Vmin[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Max_temp[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Module_Identifier_Tmax[x, m];
                }
                dt.Rows.Add(dr);
            }


            return dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string file = System.IO.Path.GetFileNameWithoutExtension(Program.form1.listView.FocusedItem.Text) + ".xlsx";
     
            string strA = "\\";
            string path = String.Concat(strA, file);//concatenate "\\" with filename
            string pathFile = String.Concat(Program.form1.txtPath.Text, path);// Program.form1.txtPath.Text is directory of excel file
   
            //These lines will open it in Excel
            ProcessStartInfo pi = new ProcessStartInfo(pathFile);
            Process.Start(pi);
        }

        private void worksheetsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string file = System.IO.Path.GetFileNameWithoutExtension(Program.form1.listView.FocusedItem.Text) + ".xlsx";
            string strA = "\\";
            string path = String.Concat(strA, file);//concatenate "\\" with filename

            string pathFile = String.Concat(Program.form1.txtPath.Text, path);// Program.form1.txtPath.Text is directory of excel file
            //Display the data from the selected Worksheet
            string sourceConnectionString = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES'", pathFile);

            OleDbDataAdapter adapter = new OleDbDataAdapter(String.Format("SELECT * FROM [{0}]", worksheetsComboBox.SelectedItem.ToString()), sourceConnectionString);
            DataTable currentSheet = new DataTable();
            adapter.Fill(currentSheet);
            adapter.Dispose();
            
            Program.form1.dataGridView1.DataSource = currentSheet;
        }

        /*
         private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
         {
             string PathConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + textBox_path.Txt + ";Extended Properties = \"Excel 12.0 Xml;HDR=YES\"; ";
             OleDbConnection conn = new OleDbConnection(PathConn);
             OleDbDataAdapter myDataAdapter = new OleDbDataAdapter("");
             DataTable dt = new DataTable();

             myDataAdapter.Fill(dt);
             dataGridView1.DataSource = dt;
         }
        */
    }

    /* Objects for section 1 and section 2 */
    static class MemoryParser
    {
        public static Readbuffer MemoryMuncher = new Readbuffer();
        public static Readbuffer MemoryMuncherB = new Readbuffer();
    }

    /* Read the S19 file */
    /* new class scope outside form */
    public class readfile
    {
        const int COLUMN_WIDTH = 8;
        public Boolean BIG_ENDIAN = false;
        static ulong address;
        static int column_width = 0;
        static byte[] memory;
        static UInt32 startAddress;
        static UInt32 sectionIndex = 0;
        static UInt32 byteCounter = 0;
        public readfile(string readFName,
            Int32 fileSize,
            UInt32 sAddress)
        {
            int first_data_byte;
            int in_byte_count;
            uint address_size;
            ulong endAddress;
            string inputline;
            startAddress = sAddress;
            memory = new byte[fileSize];
            //fill memory
            for (int i = 0; i < fileSize; i++)
            {
                memory[i] = 0;
            }
            StreamReader readStream = File.OpenText(readFName);
            while ((inputline = readStream.ReadLine()) != null)
            {
                if (inputline.Substring(0, 1) == "S")  /* ignore lines not starting */
                /* the letter 'S'            */
                {
                    switch (inputline.Substring(1, 1))
                    {
                        case "1":
                            first_data_byte = 8;
                            address_size = 4;
                            address = Convert.ToUInt64(
                                inputline.Substring(4, 4), 16);
                            convert_line(1, inputline, address_size);
                            break;
                        case "2":
                            first_data_byte = 10;
                            address_size = 6;
                            address = Convert.ToUInt64(
                                inputline.Substring(4, 6), 16);
                            convert_line(2, inputline, address_size);
                            break;
                        case "3":
                            first_data_byte = 12;
                            address_size = 8;
                            address = Convert.ToUInt64(
                                 inputline.Substring(4, 8), 16);
                            //Console.WriteLine("int: {0}", address);
                            convert_line(2, inputline, address_size);
                            break;

                        default: break;
                    }
                }

            }
            endAddress = address;/* done reading the whole S19 file */
            readStream.Close();

            /*******************************************************************************/

            /* read memory buffer, assign values based on type to class member variables for section 1*/
            Readbuffer MemoryMuncher = new Readbuffer();
            UInt32 parseAddress = startAddress;
            //byte[] varTypeArray = new byte[] {4,2,2,4,4,6,6,2,2,10,10,4,4,1,160,4,4,3,4,1,1,2,1,1,1,2,4,4,4,4,2,
            //                                   2,4,2,4,1,4,1,4,1,10,4,4,2,2,1,1,1,1,1,1,1,22,2};

     /*       if (BitConverter.IsLittleEndian)
                Array.Reverse(memory); */

            Int32 incrAddr = 0;
            /* start pulling values from memory buffer till all class members are filled */
           //while (parseAddress <= endAddress)
            {

                /* assign vars based on type by calling bitconverter */

                for (int m = 0; m < 26; m++)
                {
                    UInt32 i13 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Version_bbx[m] = i13;
                    incrAddr += 4;
                    //Console.WriteLine("Hello world");
                    //Console.WriteLine(i13);

                    UInt16 i1 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Sector_Num[m] = i1;
                    incrAddr += 2;


                    UInt16 i2 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Sector_Cur[m] = i2;
                    incrAddr += 2;

                    UInt32 i14 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Creation_Date[m] = i14;
                    incrAddr += 4;

                    UInt32 i15 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Update_Date[m] = i15;
                    incrAddr += 4;

                    float i31 = BitConverter.ToInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.PlusActualWear[m] = i31;
                    incrAddr += 4;

                    float i32 = BitConverter.ToInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.MinusActualWear[m] = i32;
                    incrAddr += 4;

                    UInt16 i3 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.PlusOpeningClosingNum[m] = i3;
                    incrAddr += 2;

                    UInt16 i4 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.MinusOpeningClosingNum[m] = i4;
                    incrAddr += 2;

                    for (int j = 0; j < 5; j++)
                    {
                        UInt16[] data = new UInt16[5];
                        UInt16 data1 = BitConverter.ToUInt16(memory, incrAddr);
                        data[j] = data1;
                        MemoryParser.MemoryMuncher.PlusOpenClosebyCurrent[j,m] = data[j];
                        incrAddr += 2;
                    }

                    for (int j = 0; j < 5; j++)
                    {
                        UInt16[] data = new UInt16[5];
                        UInt16 data1 = BitConverter.ToUInt16(memory, incrAddr);
                        data[j] = data1;
                        MemoryParser.MemoryMuncher.MinusOpenClosebyCurrent[j,m] = data[j];
                        incrAddr += 2;
                    }

                    UInt32 i16 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Bmm_pn[m] = i16;
                    incrAddr += 4;
                    //Console.WriteLine("BMM Part Nummber :");
                    //int origValue = (int)i16;
                    //Console.WriteLine(i16);


                    UInt32 i17 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Bmm_sn[m] = i17;
                    incrAddr += 4;

                    MemoryParser.MemoryMuncher.Smu_count[m] = memory[incrAddr];
                    incrAddr++;

                    for (int j = 0; j < 40; j++)
                    {
                        UInt32[] data = new UInt32[40];
                        UInt32 data1 = BitConverter.ToUInt32(memory, incrAddr);
                        data[j] = data1;
                        MemoryParser.MemoryMuncher.Smu_sn_Table[j,m] = data[j];
                        incrAddr += 4;
                    }

                    UInt32 i18 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Battery_pn[m] = i18;
                    incrAddr += 4;


                    UInt32 i19 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Battery_sn[m] = i19;
                    incrAddr += 4;

                    for (int j = 0; j < 3; j++)
                    {
                        MemoryParser.MemoryMuncher.Sw_version[j,m] = memory[incrAddr];
                        incrAddr++;
                    }
                    /**************************/
                    for (int k = 0; k < 60; k++)
                    {
                        UInt32 i20 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Time_stamp[m, k] = i20;
                        incrAddr += 4;

                        MemoryParser.MemoryMuncher.Bmm_mode[m, k] = memory[incrAddr];
                        incrAddr++;

                        MemoryParser.MemoryMuncher.Triggering_event[m, k] = memory[incrAddr];
                        incrAddr++;

                        UInt16 i5 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Event_act_Counter[m, k] = i5;
                        incrAddr += 2;

                        MemoryParser.MemoryMuncher.Soc_min[m, k] = memory[incrAddr];
                        incrAddr++;

                        MemoryParser.MemoryMuncher.Soc_max[m, k] = memory[incrAddr];
                        incrAddr++;

                        MemoryParser.MemoryMuncher.Soc_avg[m, k] = memory[incrAddr];
                        incrAddr++;

                        UInt16 i6 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Soh[m, k] = i6;
                        incrAddr += 2;

                        UInt32 i21 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Cur_Avg_High_Range[m, k] = i21;
                        incrAddr += 4;

                        UInt32 i22 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Cur_Avg_Low_Range[m, k] = i22;
                        incrAddr += 4;

                        UInt32 i23 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Amp_throughput_highrange[m, k] = i23;
                        incrAddr += 4;

                        UInt32 i24 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Amp_throughput_lowrange[m, k] = i24;
                        incrAddr += 4;

                        UInt16 i7 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Battery_voltage[m, k] = i7;
                        incrAddr += 2;

                        UInt16 i8 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Max_cell_volt[m, k] = i8;
                        incrAddr += 2;


                        UInt32 i25 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Module_Identifier_Vmax[m, k] = i25;
                        incrAddr += 4;

                        UInt16 i9 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Min_cell_voltage[m, k] = i9;
                        incrAddr += 2;

                        UInt32 i26 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Module_Identifier_Vmin[m, k] = i26;
                        incrAddr += 4;

                        MemoryParser.MemoryMuncher.Max_temp[m, k] = (sbyte)memory[incrAddr];
                        incrAddr++;

                        UInt32 i27 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Module_Identifier_Tmax[m, k] = i27;
                        incrAddr += 4;

                        MemoryParser.MemoryMuncher.Min_temp[m, k] = (sbyte)memory[incrAddr];
                        incrAddr++;

                        UInt32 i28 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Module_Identifier_Tmin[m, k] = i28;
                        incrAddr += 4;

                        MemoryParser.MemoryMuncher.Avg_temp[m, k] = (sbyte)memory[incrAddr];
                        incrAddr++;

                        for (int j = 0; j < 10; j++)
                        {
                            MemoryParser.MemoryMuncher.alarms[j, m, k] = memory[incrAddr];
                            incrAddr++;
                        }

                        UInt32 i29 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Pbit[m, k] = i29;
                        incrAddr += 4;

                        UInt32 i30 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Cbit[m, k] = i30;
                        incrAddr += 4;

                        UInt16 i10 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Ibit[m, k] = i10;
                        incrAddr += 2;

                        UInt16 i11 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.POffBit[m, k] = i11;
                        incrAddr += 2;

                        MemoryParser.MemoryMuncher.Contactor_status_main_pos[m,k] = memory[incrAddr];
                        incrAddr++;
                    }

                    //MemoryParser.MemoryMuncher.Contactor_status_main_neg = memory[incrAddr];

                    //MemoryParser.MemoryMuncher.Contactor_status_precharge = memory[incrAddr];

                    //MemoryParser.MemoryMuncher.Contactor_status_opt1 = memory[incrAddr];

                    //MemoryParser.MemoryMuncher.Contactor_status_opt2 = memory[incrAddr];

                    //MemoryParser.MemoryMuncher.Fuse_status1 = memory[incrAddr];

                    //MemoryParser.MemoryMuncher.Fuse_status2 = memory[incrAddr];
                    //incrAddr += 4592;
                    /**********************/
                    for (int j = 0; j < 22; j++)
                    {
                        MemoryParser.MemoryMuncher.Padding[j,m] = memory[incrAddr];
                        incrAddr++;
                    }

                    UInt16 i12 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Crc16[m] = i12;
                    incrAddr += 2;
                }

                parseAddress += (UInt32)incrAddr;
                
            }

            /*******************************************************************************/
           

            /* read memory buffer, assign values based on type to class member variables for section 2*/
            Readbuffer MemoryMuncherB = new Readbuffer();
            //startAddress = 0x080E0000;
            UInt32 parseAddressB = startAddress;
            
            /*       if (BitConverter.IsLittleEndian)
                       Array.Reverse(memory); */

            Int32 incrAddrB = (Int32)sectionIndex;
            /* start pulling values from memory buffer till all class members are filled */
            //while (parseAddress <= endAddress)
            {

                /* assign vars based on type by calling bitconverter */
                /**********************/
                for (int m = 0; m < 26; m++)
                {
                    UInt32 i13 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Version_bbx[m] = i13;
                    incrAddrB += 4;
                    //Console.WriteLine("Hello world");
                    //Console.WriteLine(i13);

                    UInt16 i1 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Sector_Num[m] = i1;
                    incrAddrB += 2;


                    UInt16 i2 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Sector_Cur[m] = i2;
                    incrAddrB += 2;

                    UInt32 i14 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Creation_Date[m] = i14;
                    incrAddrB += 4;

                    UInt32 i15 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Update_Date[m] = i15;
                    incrAddrB += 4;

                    float i31 = BitConverter.ToInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.PlusActualWear[m] = i31;
                    incrAddrB += 4;

                    float i32 = BitConverter.ToInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.MinusActualWear[m] = i32;
                    incrAddrB += 4;

                    UInt16 i3 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.PlusOpeningClosingNum[m] = i3;
                    incrAddrB += 2;

                    UInt16 i4 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.MinusOpeningClosingNum[m] = i4;
                    incrAddrB += 2;

                    for (int j = 0; j < 5; j++)
                    {
                        UInt16[] data = new UInt16[5];
                        UInt16 data1 = BitConverter.ToUInt16(memory, incrAddrB);
                        data[j] = data1;
                        MemoryParser.MemoryMuncherB.PlusOpenClosebyCurrent[j,m] = data[j];
                        incrAddrB += 2;
                    }

                    for (int j = 0; j < 5; j++)
                    {
                        UInt16[] data = new UInt16[5];
                        UInt16 data1 = BitConverter.ToUInt16(memory, incrAddrB);
                        data[j] = data1;
                        MemoryParser.MemoryMuncherB.MinusOpenClosebyCurrent[j,m] = data[j];
                        incrAddrB += 2;
                    }

                    UInt32 i16 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Bmm_pn[m] = i16;
                    incrAddrB += 4;
                    //Console.WriteLine("BMM Part Nummber :");
                    //int origValue = (int)i16;
                    //Console.WriteLine(i16);


                    UInt32 i17 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Bmm_sn[m] = i17;
                    incrAddrB += 4;

                    MemoryParser.MemoryMuncherB.Smu_count[m] = memory[incrAddrB];
                    incrAddrB++;

                    for (int j = 0; j < 40; j++)
                    {
                        UInt32[] data = new UInt32[40];
                        UInt32 data1 = BitConverter.ToUInt32(memory, incrAddrB);
                        data[j] = data1;
                        MemoryParser.MemoryMuncherB.Smu_sn_Table[j,m] = data[j];
                        incrAddrB += 4;
                    }

                    UInt32 i18 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Battery_pn[m] = i18;
                    incrAddrB += 4;


                    UInt32 i19 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Battery_sn[m] = i19;
                    incrAddrB += 4;

                    for (int j = 0; j < 3; j++)
                    {
                        MemoryParser.MemoryMuncherB.Sw_version[j,m] = memory[incrAddrB];
                        incrAddrB++;
                    }

                    for (int k = 0; k < 60; k++)
                    {
                        UInt32 i20 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Time_stamp[m, k] = i20;
                        incrAddrB += 4;

                        MemoryParser.MemoryMuncherB.Bmm_mode[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        MemoryParser.MemoryMuncherB.Triggering_event[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        UInt16 i5 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Event_act_Counter[m, k] = i5;
                        incrAddrB += 2;

                        MemoryParser.MemoryMuncherB.Soc_min[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        MemoryParser.MemoryMuncherB.Soc_max[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        MemoryParser.MemoryMuncherB.Soc_avg[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        UInt16 i6 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Soh[m, k] = i6;
                        incrAddrB += 2;

                        UInt32 i21 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Cur_Avg_High_Range[m, k] = i21;
                        incrAddrB += 4;

                        UInt32 i22 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Cur_Avg_Low_Range[m, k] = i22;
                        incrAddrB += 4;

                        UInt32 i23 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Amp_throughput_highrange[m, k] = i23;
                        incrAddrB += 4;

                        UInt32 i24 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Amp_throughput_lowrange[m, k] = i24;
                        incrAddrB += 4;

                        UInt16 i7 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Battery_voltage[m, k] = i7;
                        incrAddrB += 2;

                        UInt16 i8 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Max_cell_volt[m, k] = i8;
                        incrAddrB += 2;


                        UInt32 i25 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Module_Identifier_Vmax[m, k] = i25;
                        incrAddrB += 4;

                        UInt16 i9 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Min_cell_voltage[m, k] = i9;
                        incrAddrB += 2;

                        UInt32 i26 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Module_Identifier_Vmin[m, k] = i26;
                        incrAddrB += 4;

                        MemoryParser.MemoryMuncherB.Max_temp[m, k] = (sbyte)memory[incrAddrB];
                        incrAddrB++;

                        UInt32 i27 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Module_Identifier_Tmax[m, k] = i27;
                        incrAddrB += 4;

                        MemoryParser.MemoryMuncherB.Min_temp[m, k] = (sbyte)memory[incrAddrB];
                        incrAddrB++;

                        UInt32 i28 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Module_Identifier_Tmin[m, k] = i28;
                        incrAddrB += 4;

                        MemoryParser.MemoryMuncherB.Avg_temp[m, k] = (sbyte)memory[incrAddrB];
                        incrAddrB++;

                        for (int j = 0; j < 10; j++)
                        {
                            MemoryParser.MemoryMuncherB.alarms[j, m, k] = memory[incrAddrB];
                            incrAddrB++;
                        }

                        UInt32 i29 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Pbit[m, k] = i29;
                        incrAddrB += 4;

                        UInt32 i30 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Cbit[m, k] = i30;
                        incrAddrB += 4;

                        UInt16 i10 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Ibit[m, k] = i10;
                        incrAddrB += 2;

                        UInt16 i11 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.POffBit[m, k] = i11;
                        incrAddrB += 2;

                        MemoryParser.MemoryMuncherB.Contactor_status_main_pos[m, k] = memory[incrAddrB];
                        incrAddrB++;
                    }

                    //MemoryParser.MemoryMuncherB.Contactor_status_main_neg = memory[incrAddrB];

                    //MemoryParser.MemoryMuncherB.Contactor_status_precharge = memory[incrAddrB];

                    //MemoryParser.MemoryMuncherB.Contactor_status_opt1 = memory[incrAddrB];

                    //MemoryParser.MemoryMuncherB.Contactor_status_opt2 = memory[incrAddrB];

                    //MemoryParser.MemoryMuncherB.Fuse_status1 = memory[incrAddrB];

                    //MemoryParser.MemoryMuncherB.Fuse_status2 = memory[incrAddrB];
                    //incrAddrB += 4592;
                    /**********************/
                    for (int j = 0; j < 22; j++)
                    {
                        MemoryParser.MemoryMuncherB.Padding[j,m] = memory[incrAddrB];
                        incrAddrB++;
                    }

                    UInt16 i12 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Crc16[m] = i12;
                    incrAddrB += 2;
                }

                parseAddressB += (UInt32)incrAddrB;
            }

            /*******************************************************************************/


            /*   FileStream writeFile = new FileStream(
                   writeFName,
                   FileMode.OpenOrCreate);
               writeFile.Write(memory, 0, memory.Length);
               writeFile.Flush();
               writeFile.Close(); */


        }
        /* Getting the S19 string from memory */
        void convert_line(uint numberBytes, string inputline, uint address_size)
        {
            uint columncount;
            int in_byte_count;
            short data_word=0;
            in_byte_count = Convert.ToInt16(
                 inputline.Substring(2, 2), 16);

            for (columncount = address_size + 4;
               columncount < (2*in_byte_count)+4;
               columncount += numberBytes)
            {
                switch (numberBytes)
                {
                    case 1:
                        data_word = Convert.ToByte(
                           inputline.Substring((int)columncount, 1), 16);
                        memory[address - startAddress] = (byte)(data_word & 0xff);
                        break;
                    case 2:
                        data_word = Convert.ToInt16(
                           inputline.Substring((int)columncount, 2), 16);                        
                        if(!BIG_ENDIAN)
                        {
                           memory[address - startAddress] = (byte)(data_word & 0xff);
                        }
                        byteCounter++;
                        //byteCounter = byteCounter - (address_size + 4);
                        if (address == 0x080E0000)
                        {
                            /* 0x2001 is the number of lines from 0x80c0000, needs to subtract from byteCounter */
                            sectionIndex = (byteCounter - 0x2001);
                        }
                        /*
                        if (BIG_ENDIAN)
                        {
                            memory[address - startAddress] = (byte)((data_word >> 2) & 0xff);
                            memory[address - startAddress + 1] = (byte)(data_word & 0xff);
                        }
                        else
                        {
                            memory[address - startAddress] = (byte)(data_word & 0xff);
                            memory[address - startAddress + 1] = (byte)((data_word >> 2) & 0xff);
                        }
                        */
                        break;
                    case 4:
                        data_word = Convert.ToInt16(
                           inputline.Substring((int)columncount, 8), 16);
                        if (BIG_ENDIAN)
                        {
                            memory[address - startAddress] = (byte)((data_word >> 6) & 0xff);
                            memory[address - startAddress + 1] = (byte)((data_word >> 4) & 0xff);
                            memory[address - startAddress + 2] = (byte)((data_word >> 2) & 0xff);
                            memory[address - startAddress + 3] = (byte)(data_word & 0xff);
                        }
                        else
                        {
                            memory[address - startAddress] = (byte)(data_word & 0xff);
                            memory[address - startAddress + 1] = (byte)((data_word >> 2) & 0xff);
                            memory[address - startAddress + 2] = (byte)((data_word >> 4) & 0xff);
                            memory[address - startAddress + 3] = (byte)((data_word >> 6) & 0xff);
                        }
                        break;
                }
                //test code for writing to console
                //Console.Write("The size of char is {0}.", sizeof(Char));
                //Console.WriteLine("SHARNA", '\n');
               // Console.Write(address.ToString("X2"));
               // Console.Write("/" + data_word.ToString("X2") + ";");
               
                column_width++;
                if(column_width == COLUMN_WIDTH)
                {
                    Console.WriteLine("");
                    column_width = 0;
                }
                address += numberBytes/2; 
            }
        }
    }
}

/* Class members to store the S19 data from memory parser */ 
public class Readbuffer
{
    const UInt32 uiSixtySeconds = 60;

    public UInt32[] Version_bbx = new UInt32[26];
    public UInt16[] Sector_Num = new UInt16[26];
    public UInt16[] Sector_Cur = new UInt16[26];
    public UInt32[] Creation_Date = new UInt32[26];
    public UInt32[] Update_Date = new UInt32[26];
    public float[] PlusActualWear = new float[26];
    public float[] MinusActualWear = new float[26];
    public UInt16[] PlusOpeningClosingNum = new UInt16[26];
    public UInt16[] MinusOpeningClosingNum = new UInt16[26];
    public UInt16[,] PlusOpenClosebyCurrent = new UInt16[5,26];
    public UInt16[,] MinusOpenClosebyCurrent = new UInt16[5,26];
    public UInt32[] Bmm_pn = new UInt32[26];
    public UInt32[] Bmm_sn = new UInt32[26];
    public byte[] Smu_count = new byte[26];
    public UInt32[,] Smu_sn_Table = new UInt32[40,26];
    public UInt32[] Battery_pn = new UInt32[26];
    public UInt32[] Battery_sn = new UInt32[26];
    public byte[,] Sw_version = new byte[3,26];

    public UInt32[,] Time_stamp = new UInt32[26,uiSixtySeconds];
    public byte[,] Bmm_mode = new byte[26, uiSixtySeconds];
    public byte[,] Triggering_event = new byte[26, uiSixtySeconds];
    public UInt16[,] Event_act_Counter = new UInt16[26, uiSixtySeconds];
    public byte[,] Soc_min = new byte[26, uiSixtySeconds];
    public byte[,] Soc_max = new byte[26, uiSixtySeconds];
    public byte[,] Soc_avg = new byte[26, uiSixtySeconds];
    public UInt16[,] Soh = new UInt16[26, uiSixtySeconds];
    public UInt32[,] Cur_Avg_High_Range = new UInt32[26, uiSixtySeconds];
    public UInt32[,] Cur_Avg_Low_Range = new UInt32[26, uiSixtySeconds];
    public UInt32[,] Amp_throughput_highrange = new UInt32[26, uiSixtySeconds];
    public UInt32[,] Amp_throughput_lowrange = new UInt32[26, uiSixtySeconds];
    public UInt16[,] Battery_voltage = new UInt16[26, uiSixtySeconds];
    public UInt16[,] Max_cell_volt = new UInt16[26, uiSixtySeconds];
    public UInt32[,] Module_Identifier_Vmax = new UInt32[26, uiSixtySeconds];
    public UInt16[,] Min_cell_voltage = new UInt16[26, uiSixtySeconds];
    public UInt32[,] Module_Identifier_Vmin = new UInt32[26, uiSixtySeconds];
    public sbyte[,] Max_temp = new sbyte[26, uiSixtySeconds];
    public UInt32[,] Module_Identifier_Tmax = new UInt32[26, uiSixtySeconds];
    public sbyte[,] Min_temp = new sbyte[26, uiSixtySeconds];
    public UInt32[,] Module_Identifier_Tmin = new UInt32[26, uiSixtySeconds];
    public sbyte[,] Avg_temp = new sbyte[26, uiSixtySeconds];
    public byte[,,] alarms = new byte[10,26, uiSixtySeconds];
    public UInt32[,] Pbit = new UInt32[26, uiSixtySeconds];
    public UInt32[,] Cbit = new UInt32[26, uiSixtySeconds];
    public UInt16[,] Ibit = new UInt16[26, uiSixtySeconds];
    public UInt16[,] POffBit = new UInt16[26, uiSixtySeconds];
    public byte[,] Contactor_status_main_pos = new byte[26, uiSixtySeconds];
    //public byte[,] Contactor_status_main_neg = new byte[26, uiSixtySeconds];
    //public byte[,] Contactor_status_precharge = new byte[26, uiSixtySeconds];
    //public byte[,] Contactor_status_opt1 = new byte[26, uiSixtySeconds];
    //public byte[,] Contactor_status_opt2 = new byte[26, uiSixtySeconds];
    //public byte[,] Fuse_status1 = new byte[26, uiSixtySeconds];
    //public byte[,] Fuse_status2 = new byte[26, uiSixtySeconds];

    public byte[,] Padding = new byte[22,26];
    public UInt16[] Crc16 = new UInt16[26];
}

