using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace DesktopApp1
{
    public partial class Form1 : Form
    {
        List<string> listFiles = new List<string>();
        private object txtPath;
        private object imageList;

        public object Directory { get; private set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Click on the link below to continue learning how to build a desktop app using WinForms!
            System.Diagnostics.Process.Start("http://aka.ms/dotnet-get-started-desktop");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Thanks!");
            
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {
        }

        private void folderBrowserDialog2_HelpRequest(object sender, EventArgs e)
        {
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = "bin";
            ofd.Filter = "Trionic 8 binary files|*.bin;*.s19";
            ofd.Multiselect = false;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string filename = string.Empty;
                if (ofd.FileName.ToUpper().EndsWith("S19"))
                {
                    srec2bin cvt = new srec2bin();

                   // cvt.ConvertSrecToBin(ofd.FileName, out filename);
                }
                else
                {
                    filename = ofd.FileName;
                }
               // OpenFile(filename);
            }
        }
    }
}
